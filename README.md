# Duo Auto Approve

An android application to approve duo requests automatically. 

This application simply listens to your notifications and approves duo requests in the background. We believe source code should be public, since it requires such an important permission. 

However, if you do not want to build it yourself, you can [download](/duoautoapprove.apk) the latest apk from the root directory.