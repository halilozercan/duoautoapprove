package com.halilibo.duoautoapprove

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        val notificationListenerString = Settings.Secure.getString(this.contentResolver, "enabled_notification_listeners")
        //Check notifications access permission
        if (notificationListenerString == null || !notificationListenerString.contains(packageName)) {
            button.visibility = View.VISIBLE
            textview.visibility = View.GONE
            button.text = "Grant Permissions to catch Duo Notification"
            button.setOnClickListener {
                startActivity(Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"))
            }
        } else {
            button.visibility = View.GONE
            textview.visibility = View.VISIBLE
            textview.text = "Good to go!!"
        }
    }
}
