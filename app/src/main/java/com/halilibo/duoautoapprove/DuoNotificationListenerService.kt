package com.halilibo.duoautoapprove

import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.util.Log

class DuoNotificationListenerService : NotificationListenerService() {

    override fun onNotificationPosted(sbn: StatusBarNotification) {
        if(sbn.packageName == "com.duosecurity.duomobile") {
            sbn.notification.actions?.let { actions ->
                if(actions.size == 1) {
                    Log.d("Notification", "clicked on first")
                    actions[0].actionIntent.send()
                }
                else if(actions.size == 2) {
                    Log.d("Notification", "clicked on second")
                    actions[0].actionIntent.send()
                }
            }
        }
        Log.d("Notification", "we got something")
    }
}